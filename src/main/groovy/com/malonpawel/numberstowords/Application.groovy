package com.malonpawel.numberstowords

class Application {

  static void main(String[] args) {

    CliBuilder cli = createCli()
    OptionAccessor options = cli.parse(args)

    if (!options|| options.h) {
      cli.usage
      return
    }

    NumberToWordsConverter numberToWordsConverter = new NumberToWordsConverter()
    String numberAsWords = numberToWordsConverter.convert(getNumberToConvert(options))
    println numberAsWords
  }

  private static CliBuilder createCli() {
    CliBuilder cli = new CliBuilder(
      usage: "Application -n numberToConvert"
    )
    cli.with {
      h(longOpt: 'help', 'Usage Information', required: false)
      n(longOpt: 'number', 'Number to convert', args: 1, required: true)
    }
    cli
  }

  private static int getNumberToConvert(OptionAccessor options) {
    String number = options.n

    if (!number.isNumber()) {
      throw new IllegalArgumentException("${number} is not a valid number")
    }
    number as int
  }


}
