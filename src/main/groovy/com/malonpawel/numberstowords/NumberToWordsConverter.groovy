package com.malonpawel.numberstowords

class NumberToWordsConverter {

  private static final List<String> ONES = [
    '',
    'one',
    'two',
    'three',
    'four',
    'five',
    'six',
    'seven',
    'eight',
    'nine',
    'ten',
    'eleven',
    'twelve',
    'thirteen',
    'fourteen',
    'fifteen',
    'sixteen',
    'seventeen',
    'eighteen',
    'nineteen'
  ]

  private static final List<String> TENS = [
    '',
    '',
    'twenty',
    'thirty',
    'forty',
    'fifty',
    'sixty',
    'seventy',
    'eighty',
    'ninety'
  ]

  private static final List<String> SCALES = [
    '',
    'thousand',
    'million',
    'billion'
  ]

  String convert(int number) {
    List<String> words = []
    (0..getThousandsExponent(number)).each { Integer iteration ->
      prependWord(words, SCALES[iteration])
      prependWord(words, getTensAndOnes(number % 100))
      prependWord(words, getHundreds(number % 1000))

      number /= 1000
    }
    words.join(' ')
  }

  public int getThousandsExponent(int number) {
    log10(number) / 3
  }

  private int log10(int number) {
    Math.log10(Math.abs(number))
  }

  private String getTensAndOnes(int number) {
    number < 20 ? getLowerThan20(number) : getHigherOrEqual20(number)
  }

  private String getLowerThan20(int number) {
    getOnes(number)
  }

  private String getHigherOrEqual20(int number) {
    [getTens(getTensCount(number % 100)), getOnes(number % 10)].findAll().join(' ')
  }

  private int getTensCount(int number) {
    number / 10
  }

  private String getTens(int tensCount) {
    tensCount > 1 ? TENS[tensCount] : ''
  }

  private String getOnes(int onesValue) {
    ONES[onesValue] ?: ''
  }

  private String getHundreds(int number) {
    int hundredsCount = getHundredsCount(number)
    hundredsCount > 0 ? "${ONES[hundredsCount]} hundred" : ''
  }

  private int getHundredsCount(int number) {
    number / 100
  }

  private void prependWord(List<String> words, String word) {
    if (word) {
      prependAndJoinerIfNeeded(words)
      words.add(0, word)
    }
  }

  private void prependAndJoinerIfNeeded(List<String> words) {
    if (words.size() == 1) {
      words.add(0, 'and')
    }
  }
}
