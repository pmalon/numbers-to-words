package com.malonpawel.numberstowords

import spock.lang.Specification
import spock.lang.Unroll

class NumberToWordsConverterTest extends Specification {

  @Unroll
  def "should convert number #number to words '#expectedWords'"() {
    given:
    NumberToWordsConverter converter = new NumberToWordsConverter()

    when:
    String numberAsWords = converter.convert(number)

    then:
    numberAsWords == expectedWords

    where:
    number    | expectedWords
    1         | 'one'
    19        | 'nineteen'
    21        | 'twenty one'
    55        | 'fifty five'
    119       | 'one hundred and nineteen'
    1_001     | 'one thousand and one'
    2_001     | 'two thousand and one'
    1_111     | 'one thousand one hundred and eleven'
    10_111    | 'ten thousand one hundred and eleven'
    12_345    | 'twelve thousand three hundred and forty five'
    123_455   | 'one hundred twenty three thousand four hundred and fifty five'
    6_500_100 | 'six million five hundred thousand and one hundred'
  }
}
