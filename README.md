# Number to words converter


## Project setup
The project comes ready with an instance of Gradle wrapper so no installation of build system is required.
 
### 3rd party dependencies
  
- Java 1.7 or newer


## Testing
To run the tests for the project execute the following command from within the project's root directory:


```
./gradlew clean test
```

## Running
To run the the app execute the following command from within the project's root directory:


```
./gradlew clean run -PappArgs="-n 1234"
```

where `1234` is a number to be converted to words